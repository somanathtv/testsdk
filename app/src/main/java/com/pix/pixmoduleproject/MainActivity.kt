package com.pix.pixmoduleproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
//import com.pix.pixsdk.TestMsg


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //TestMsg.printMe(this,"This msg from module!")
    }
}